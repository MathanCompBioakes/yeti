from django.conf.urls import patterns, include, url
from django.contrib import admin
from . import views

urlpatterns = [
    # Examples:
    url(r'^$', views.index, name='index'),
    url(r'^about/', views.about, name='about'),
    url(r'^add_event/', views.AddEventView.as_view(), name='add_event'),
    url(r'^profile/', views.user_profile, name='user_profile'),
    url(r'^search/', views.search, name='search_events'),
    url(r'^logout/$', views.logout),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url('', include('django.contrib.auth.urls', namespace='auth')),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
]
