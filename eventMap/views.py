from django.shortcuts import render, redirect
from django.template.context import RequestContext
from django.views.generic import FormView
from models import Event
from ipware.ip import get_real_ip
from django.http import HttpRequest
from django.contrib.gis.geoip import GeoIP
from django.contrib.auth import logout as auth_logout
from django.contrib import messages
from forms import SimpleForm, EventForm
import datetime
import logging
logger = logging.getLogger(__name__)

class AddEventView(FormView):
    model = Event
    template_name = "eventMap/add_event.html"
    form_class = EventForm
    success_url = 'success'

    def form_valid(self, form):
		form.save()
		return super(AddEventView, self).form_valid(form)

    def form_invalid(self, form):
		return super(AddEventView, self).form_invalid(form)

    """def post(self, request, *args, **kwargs):
        form = EventForm(request.POST) # Pass the resuest's POST/GET data
        if form.is_valid():
            form.save()
            return redirect(self.get_success_url())"""

def index(request):
    return render(request, 'eventMap/index.html')

def about(request):
    return render(request, 'eventMap/about.html')

def user_profile(request):
    return render(request, 'eventMap/user_profile.html')

def login(request):
    return render(request, 'eventMap/login.html')

def logout(request):
    auth_logout(request)
    return redirect('/')

def submit_event(request):
    if request.method == 'POST':
        form = EventForm(request.POST) # Pass the resuest's POST/GET data
        #if form.is_valid():         # invoke .is_valid
        form.save() # l
    return redirect('/')

def search(request):
    #now = datetime.datetime.now()
    #pois = PointOfInterest.objects.all()
    if request.method == 'POST':
        form = EventForm(request.POST) # Pass the resuest's POST/GET data
        if form.is_valid():         # invoke .is_valid
    	    form.save() # l
            return redirect('/')
            #return render(request, 'eventMap/user_profile.html')
        else:
            messages.error(request, "Error")
            #return render_to_response('eventMap/form_errors.html', {'form': form})
            return render(request, 'eventMap/user_profile.html')
    user_ip = get_real_ip(request)
    if user_ip is not None:
        g = GeoIP()
        lat,lng = g.lat_lon(user_ip)
        city = g.city(user_ip)['city']
        context = RequestContext(request, {'request': request,
                                            'user': request.user,
                                            'lat': lat,
                                            'lng': lng,
                                            'city': city})
        logger.debug("context[city]={}".format(context['city']))
    return render(request, 'eventMap/search_events.html', context)
