from django.db import models
from geoposition.fields import GeopositionField

class Event(models.Model):
    event_name = models.CharField(max_length=100)
    event_description = models.TextField(max_length=400, default="Event Description")
    event_loc = GeopositionField(blank=True)
    event_date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()

    class Meta:
        verbose_name_plural = 'events'
