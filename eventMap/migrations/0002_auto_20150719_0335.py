# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventMap', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='event',
            old_name='event_end_time',
            new_name='end_time',
        ),
        migrations.RenameField(
            model_name='event',
            old_name='event_start_time',
            new_name='start_time',
        ),
        migrations.RemoveField(
            model_name='event',
            name='description',
        ),
        migrations.AddField(
            model_name='event',
            name='event_description',
            field=models.TextField(default=b'Event Description', max_length=400),
        ),
    ]
