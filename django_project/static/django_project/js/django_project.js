Map = {
	Models: {},
	Collections: {},
	Views: {},
	Loc: {
		lat : 0,
		lng : 0
	}
};

Map.Models.addEventFlag = false;

Map.template = function (name) {
	var source = $('#' + name + '-template').html();
	return Handlebars.compile(source);
};

Map.Views.GoogleMap = Backbone.View.extend({
	el: $('#map-canvas'),
	template: Map.template('grid-12'),
	initialize: function () {
		this.render();
	},
	activate: function () {
		Map.geocoder =  new google.maps.Geocoder();
		if (navigator.geolocation) {
			$.when(this.getPosition())
				.pipe(this.setCenter)
				.then(this.setMap);
		};
	},
	setMap: function() {
		var mapOptions = {
			zoom: 12,
	center: new google.maps.LatLng(Map.Loc.lat, Map.Loc.lng),
	mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var domElement = $('#googleMapBox');

		Map.map = new google.maps.Map(domElement.get(0), mapOptions);
		google.maps.event.addListener(Map.map, 'click', function(point) {
			if (point && Map.Models.addEventFlag) {
				$("#id_event_loc_0").val(point.latLng.A);
		   		$("#id_event_loc_1").val(point.latLng.F);
				Map.map.setOptions({draggableCursor:''});
				Map.Models.addEventFlag = false;
			}
		});
	},
	getPosition: function(options) {
		var deferred = $.Deferred();

		navigator.geolocation.getCurrentPosition(
				deferred.resolve,
				deferred.reject,
				options);

		return deferred.promise();
	},
	setCenter: function(position) {
		if (position) {
			Map.Loc.lat = position.coords.latitude;
			Map.Loc.lng = position.coords.longitude;
		} else {
			Map.Loc.lat = 0;
			Map.Loc.lng = 0;
		}
	},
	render: function () {
		$('#map-canvas').html(this.template(this));
		$('#search_bar').attr("placeholder","Enter City");
		this.activate();
		return this;
	},
	events: {
		"click #search_button": "searchCity",
		"enter #search_bar": "searchCity",
	},
	searchCity: function() {
		var city = $('#search_bar').val();
		Map.geocoder.geocode( { 'address': city }, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				Map.lat = results[0].geometry.location.lat();
				Map.lng = results[0].geometry.location.lng();
				Map.map.setCenter(new google.maps.LatLng(Map.lat, Map.lng));
			} else {
				alert("Something got wrong " + status);
			}
		});
		$('#search_button').blur();
		$('#search_bar').val("");
		$('#search_bar').blur();
	},
});

$(function () {
	var GoogMap = new Map.Views.GoogleMap();
	var addEventFlag = false;
	$('input').keyup(function(e){
		if(e.keyCode == 13){
			$(this).trigger('enter');
		}
	});
	$('#addEventButton').on('click', function () {
		if ( $(this).button()[0].innerText == "Search Events" ) {
			$("#side-bar").empty();
			$(this).button('reset');
		} else {
			var dfd = new $.Deferred();
			$.when(dfd)
		.then( function(){
			$("#id_event_date").datepicker({ format: "mm/dd/yyyy" });
			$("#id_start_time").timepicker({ showPeriod: true });
			$("#id_end_time").timepicker({ showPeriod: true });
			$("#id_event_loc_0").prop('disabled', true);
			$("#id_event_loc_1").prop('disabled', true);
			$("#id_add_location").on('click', function() {
				Map.map.setOptions({draggableCursor:'crosshair'});
				Map.Models.addEventFlag = true;
			});
		});
	$("#side-bar").load("/add_event", function(){
		dfd.resolve();
	});
	$(this).button('complete');
		}
	})
});

